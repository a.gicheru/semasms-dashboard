@echo off
call mvn clean package
call docker build -t com.Silicon/Sema_SMS_DashB .
call docker rm -f Sema_SMS_DashB
call docker run -d -p 9080:9080 -p 9443:9443 --name Sema_SMS_DashB com.Silicon/Sema_SMS_DashB